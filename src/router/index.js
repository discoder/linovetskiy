import Vue from 'vue'
import Router from 'vue-router'
import App from '../App'

import Home from "../components/general/home"
import About from "../components/general/about"
import Music from "../components/general/music"
import Showcasepage from "../components/general/showcasepage"
import Styleguide from "../components/general/styleguide"
import Pagenotfound from "../components/general/pagenotfound"

Vue.use(Router)

const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
  { path: '/showcase', component: Showcasepage },
  { path: '/music', component: Music },
  { path: '/styleguide', component: Styleguide },
  { path: '/*', component: Pagenotfound }
]

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: routes
})
